import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:news_app/mixins/servernews.dart';
import 'package:news_app/model/news.dart';

class NewsRepository {
  Future<List<News>> getNews(int page) async {
    http.Response value = await http.get(
      Uri.parse(ServerNews.alamatApiNews +
          "/v2/everything?q=apple&from=2022-03-21&to=2022-03-21&sortBy=popularity&apiKey=${ServerNews.apiKeyNews}" +
          "&page=$page"),
    );
    if (value.statusCode == 200) {
      Map<String, dynamic> response = json.decode(value.body);
      List<dynamic> listData = response["articles"];
      List<News> listNews =
          listData.map((dynamic element) => News.fromMap(element)).toList();
      return listNews;
    } else {
      throw Exception();
    }
  }
}
