class News {
  late String name;
  late String title;
  late String description;
  late String image;

  News({
    required this.name,
    required this.title,
    required this.description,
    required this.image,
  });

  factory News.fromMap(Map<String, dynamic> map) {
    return News(
      name: map["source"]["name"],
      title: map["title"],
      description: map["description"],
      image: map["urlToImage"],
    );
  }
}
