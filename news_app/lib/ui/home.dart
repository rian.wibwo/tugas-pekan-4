import 'package:flutter/material.dart';
import 'package:news_app/model/news.dart';
import 'package:news_app/repository/news_repository.dart';
import 'package:news_app/ui/profil_screen.dart';
import 'package:news_app/widgets/loading_view.dart';
import 'package:news_app/widgets/news_view.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<News> listNews = [];
  bool isNoLoadMore = false;
  int page = 1;
  bool isLoading = true;

  void callAPI() {
    isLoading = true;
    NewsRepository().getNews(page).then((List<News> value) {
      isLoading = false;
      if (value.isEmpty) {
        isNoLoadMore = true;
      }
      listNews = [...listNews, ...value];
      setState(() {});
    }).catchError((err, track) {
      // ignore: avoid_print
      print("Something Wring $err $track");
    });
  }

  @override
  void initState() {
    callAPI();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: const Text("Apple News"),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
                icon: const Icon(Icons.person),
                onPressed: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ProfilScreen()));
                },
                tooltip:
                    MaterialLocalizations.of(context).openAppDrawerTooltip);
          },
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: listNews.isEmpty
            ? const LoadingView()
            : NotificationListener<ScrollNotification>(
                onNotification: (ScrollNotification scrollNotification) {
                  if (scrollNotification.metrics.pixels ==
                      scrollNotification.metrics.maxScrollExtent) {
                    if (isNoLoadMore == false) {
                      if (isLoading == false) {
                        page = page + 1;
                        callAPI();
                      }
                    }
                  }
                  return true;
                },
                child: ListView.builder(
                    itemCount: isNoLoadMore == true
                        ? listNews.length
                        : listNews.length + 1,
                    itemBuilder: (BuildContext context, int index) {
                      if (index < listNews.length) {
                        return NewsView(news: listNews[index]);
                      } else {
                        return const LoadingView();
                      }
                    }),
              ),
      ),
    );
  }
}
