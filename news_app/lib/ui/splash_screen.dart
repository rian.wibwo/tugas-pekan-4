import 'dart:async';

import 'package:flutter/material.dart';
import 'package:news_app/ui/home.dart';

import 'package:news_app/ui/register_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  late SharedPreferences sharedPreferences;

  startSplashScreen() async {
    sharedPreferences = await SharedPreferences.getInstance();
    var duration = const Duration(seconds: 5);
    return Timer(duration, () {
      if (sharedPreferences.getString("email") != null) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => const Home()),
        );
      } else {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => const RegisterScreen()),
        );
      }

      ///cek session
    });
  }

  @override
  void initState() {
    startSplashScreen();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      margin: const EdgeInsets.symmetric(horizontal: 90, vertical: 300),
      child: Column(
        children: [
          Image.asset('assets/images/apple.png'),
          const SizedBox(height: 10),
          const Text(
            "Everything About Apple",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    ));
  }
}
